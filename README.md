# README #

This is just some basic info on the source atm - more details on how to setup and the database still to come.

## Tripwire - EVE Online wormhole mapping web tool ##

* [Tripwire database](https://drive.google.com/file/d/0B2nU7w1pM6WrNVc0YThXRGlZV2M/view?usp=sharing)
* [EVE_API database](https://drive.google.com/file/d/0B2nU7w1pM6WrNnRZVE94aExJd2M/view?usp=sharing)
* MIT license
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

## How do I get set up? ##
This tutorial was written and tested on Ubuntu 14.04 LTS.
It also depends on some future changes that may not have been implemented into the master branch yet, so beware of bugs.
In case you need help, feel free to contact me.

### General Setup ###
* Make sure you are on latest patches:

        sudo apt-get update && sudo apt-get upgrade


* Install AMP stack:

        sudo apt-get install apache2 php5 mysql-server-5.6


* Secure the mysql server. Be sure to choose a safe password!

        sudo mysql_secure_installation


* Install extras:

        sudo apt-get install php5-curl php5-mysqlnd git


### Setting up the database ###
* Download these databases:

    [tripwire.sql](https://drive.google.com/file/d/0B2nU7w1pM6WrNVc0YThXRGlZV2M/view?usp=sharing)
        
    [eve-api.sql](https://drive.google.com/file/d/0B2nU7w1pM6WrNnRZVE94aExJd2M/view?usp=sharing)

    You will need to download through a browser and copy these files to your server afterwards.


* Get latest SDE dump in MySQL format:

        wget https://www.fuzzwork.co.uk/dump/mysql-latest.tar.bz2


* Unzip the SDE:

        bunzip2 mysql-latest.tar.bz2
        tar -xvf mysql-latest.tar

    This will create a subdirectory with the needed sql files.


* Enter MySQL command prompt. You will need to enter the MySQL root password you entered during setup.

        mysql -u root -p


* Create a user. Be sure to change the password in the command accordingly.

        CREATE USER 'tripwire'@'localhost' IDENTIFIED BY 'changethispassword';


* Next, create the necessary tables:

        CREATE DATABASE eve_api; 
        CREATE DATABASE eve_sde;
        CREATE DATABASE tripwire;


* Grant permissions on these tables to your tripwire user:

        GRANT ALL PRIVILEGES ON eve_api.* to 'tripwire'@'localhost';
        GRANT ALL PRIVILEGES ON eve_sde.* to 'tripwire'@'localhost';
        GRANT ALL PRIVILEGES ON tripwire.* to 'tripwire'@'localhost';
        FLUSH PRIVILEGES;


* `quit` the mysql command line.

* Test the settings by logging into mysql as your newly created user:

        mysql -u tripwire -p


* Then, show the databases to verify:

        mysql> show databases;
        
        +--------------------+
        | Database           |
        +--------------------+
        | information_schema |
        | eve_api            |
        | eve_sde            |
        | tripwire           |
        +--------------------+


* Don't forget to `quit` after you're done verifying.

* Next, import the sql files you downloaded earlier, one by one.
    This will ask for your MySQL root password every time. You may have to modify the last command to fit the sde file on your disk. The last command may take a while to complete, as it is a big database. Don't worry, it'll complete after a few minutes.

        mysql -u root -p eve_api < eve_api.sql
        mysql -u root -p tripwire < tripwire.sql
        mysql -u root -p eve_sde < yc118-1-116645/mysql56-yc118-1-116645.sql


* Log into mysql as root once again, and kickstart the updater:

        mysql -u root -p
        INSERT INTO eve_api.cacheTime (type, time) VALUES ('activity', now());
        quit;


### Setting up the webserver ###
* Get Tripwire into your webserver root:

        cd /var/www
        sudo rm -r html
        sudo git clone https://bitbucket.org/daimian/tripwire.git html
        cd html


* Give the webserver user access to the files:

        sudo chown -R www-data:www-data *


* Make a file called "db.inc.php", content as follows:

        <?php
        
        // EVE SDE table name
        $eve_dump = 'eve_sde';
        
        // Set php timezone to EVE time
        date_default_timezone_set('UTC');
        
        // Initialize DB Connection
        try {
            $mysql = new PDO(
                'mysql:host=localhost;dbname=tripwire;charset=utf8',
                'tripwire',
                'changethispassword',
                Array(
                    PDO::ATTR_PERSISTENT     => true,
                    PDO::MYSQL_ATTR_LOCAL_INFILE => true
                )
            );
            // Set MySQL timezone to EVE time
            $mysql->exec("SET time_zone='+00:00';");
        } catch (PDOException $error) {
            echo 'DB error';//$error;
        }
        
        ?>



* Set up the kickstarter for api_pull.php, that allows *us* to run it, but not everyone on the internet. Put `super.php` anywhere outside the web root and make sure it's readable by everyone. (User's home directory is fine)

    Content:

        #!php
        <?php
        session_name("kickstarter");
        session_start();
        $_SESSION['super'] = "1";
        ?>


* Enable php5-curl:

        sudo php5enmod curl

    And restart apache to propagate the changes:

        sudo service apache2 restart


### Cronjob & updating ###
* Set up the cronjob to pull the api data:

    sudo crontab -u www-data -e

    A file will open. Add the following line (don't forget to edit the path to super.php, and make sure to use an absolute path):

        */3 * * * * /usr/bin/php -d auto_prepend_file=/path/to/super.php /var/www/html/tools/api_pull.php

    Save and exit when you're done.


* Now, we need to modify the api updater with a file reference. Open tools/api_pull.php in your favourite editor, and change this:

        require('../db.inc.php');
        require('../api.class.php');

    to this:

        require('/var/www/html/db.inc.php');
        require('/var/www/html/api.class.php');

    (Or whatever your webserver root is)


* Congratulations, you have set up your own instance of Tripwire! You should now be able to browse to your server's adress and test it thoroughly. Don't forget to update it regularly by using

        sudo git pull

    in your Tripwire root directory.
    Be sure to have backups at hand, in case something breaks.